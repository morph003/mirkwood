﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_attack_Trigger : MonoBehaviour
{
    public GameObject es;
    private Enemy_Stats stats;

    // Start is called before the first frame update
    void Start()
    {
        stats = es.GetComponent<Enemy_Stats>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerController>().TakeDamage(15);
        }
    }
}
