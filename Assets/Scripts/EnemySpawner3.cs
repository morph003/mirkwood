﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner3 : MonoBehaviour
{
    public GameObject Enemy3;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(transform.position.x+Random.Range(-2,2),transform.position.y + Random.Range(-2, 2),transform.position.z);
        Instantiate(Enemy3);
    }
}
