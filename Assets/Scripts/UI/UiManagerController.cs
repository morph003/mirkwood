﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiManagerController : MonoBehaviour
{
    //Mecanicas de Pausar:
    public GameObject menuPausaUI;
    public static bool Pausar;
    public PlayerController player;



    // Start is called before the first frame update
    void Start()
    {

        Pausar = false;
        Resume();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape) && player.onMenu==false)
        {
            if (Pausar)
            {

                Resume();
            }
            else
            {

                Pause();
            }

        }
    }

    void Pause()
    {
        menuPausaUI.SetActive(true);
        Time.timeScale = 0f;
        //FindObjectOfType<SoundManager>().Play("Pausa");
        Pausar = true;
        
    }
    void Resume()
    {

        Pausar = false;

        menuPausaUI.SetActive(false);
        Time.timeScale = 1f;
        
    }
}
