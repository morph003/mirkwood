﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class enemy_move : MonoBehaviour
{
    //this script meant to be in an enemy, who has to have:
    //
    //* a rigidbody
    //* a child object with a navmesh agent
    //
    //also:
    //
    //* a nav mesh in which the navmesh agent can function
    //* a collider set to "trigger" which has to call this enemy
    //

    public bool combat_mode = false;
    public bool get_moving = true;//value pasted from a variable in the patrol script
    public bool activate_chase_mode = false;
    public float chase_mode_speed = 0.2f;
    public Rigidbody own_rigidbody;
    public NavMeshAgent navagent_component;
    float navagent_base_speed;
    public Vector3 own_rigidbody_pos = new Vector3(0, 0, 0);
    public float movement_speed = 0.01f;
    //float wake_up_timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        get_moving = gameObject.GetComponent<patrol_test>().should_move;
        own_rigidbody = gameObject.GetComponentInChildren<Rigidbody>();
        own_rigidbody_pos = own_rigidbody.transform.position;
        navagent_component = gameObject.GetComponent<NavMeshAgent>();
        navagent_base_speed = navagent_component.speed;
    }

    // Update is called once per frame
    void Update()
    {
       own_rigidbody_pos = own_rigidbody.transform.position;
        get_moving = gameObject.GetComponent<patrol_test>().should_move;

        if (get_moving == true && combat_mode ==false)
        {
            //restore nav agent speed if true
            navagent_component.speed = navagent_base_speed;
        }
        else
        {

            //make navagent stop moving when in combat mode
            if (combat_mode == true)
            {
                navagent_component.speed = 0;
            }
        }

    }
}
