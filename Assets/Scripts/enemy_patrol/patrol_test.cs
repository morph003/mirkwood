﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class patrol_test : MonoBehaviour
{
    //this script meant to be in an enemy, who has to have:
    //
    //* a rigidbody
    //* a child object with a navmesh agent
    //
    //also:
    //
    //* a nav mesh in which the navmesh agent can function
    //* a collider set to "trigger" which has to call this enemy
    //

   public float change_delay = 1;//remain idle for this amount of time before going towards another patrol destination
    public float timer = 0;// being idle
    public float position_change_time_limit = 0;//how much time to wait before changing patrol destination to somewhere else (so the enemy doesnt get stuck trying to get to a point out of navmesh bounds)
    float position_change_timer = 0;
    public bool run_timer = false;//run idle timer
    public Vector3 target_position = new Vector3(11.53f, 1.143333f, -1.56f);//the enemy´s next patrol destination
    Vector3 og_pos = new Vector3(0, 0, 0);//enemy´s starting location
    public Vector2 patrol_x_range = new Vector2(-1,1);
    public Vector2 patrol_z_range = new Vector2(-1,1);
    public NavMeshAgent navagent_component;
    float navagent_base_speed;
    public bool should_move = false; //the enemy keeps moving in patrol mode
    public bool keep_moving = false; //the enemy doesnt stop to change direction every "position_change_time_limit" seconds

    // Start is called before the first frame update
    void Start()
    {
       og_pos = new Vector3(this.gameObject.transform.position.x, 0, this.gameObject.transform.position.z);
        target_position = new Vector3(og_pos.x + Random.Range(patrol_x_range.x,patrol_x_range.y), 0 , og_pos.z + Random.Range(patrol_z_range.x,patrol_z_range.y));
        navagent_component = gameObject.GetComponent<NavMeshAgent>();
        navagent_component.destination = target_position;
        navagent_base_speed = navagent_component.speed;
        should_move = true;
    }

    // Update is called once per frame
    void Update()
    {
      if (navagent_component.transform.position.x == target_position.x && navagent_component.transform.position.z == target_position.z)
        {
            run_timer = true;
        }
      if (run_timer == true)
        {
            should_move = false; navagent_component.speed = 0;
           timer += Time.deltaTime;
            if (timer >= change_delay)
            {
                run_timer = false;
                timer = 0;
                target_position = new Vector3(this.gameObject.transform.position.x + Random.Range(patrol_x_range.x,patrol_x_range.y), 0, this.gameObject.transform.position.z + Random.Range(patrol_z_range.x,patrol_z_range.y));
                navagent_component.destination = target_position;

                should_move = true; navagent_component.speed = navagent_base_speed;
            }
        }
      if (should_move == true)
        {
            if (keep_moving == false)
            {
                if (position_change_timer >= position_change_time_limit)//is the enemy taking too long to reach its patrol destination
                {
                    position_change_timer = 0;
                    run_timer = true;
                }
                else
                {
                    position_change_timer += Time.deltaTime;
                }
            }
            else
            {
                position_change_timer = position_change_time_limit;
            }
        }

   
    }
}
