﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestGolemWeaponTransformHandler : MonoBehaviour
{
    public float hold_time = 1;
    bool normal_attack = true;
    float timer_time = 0;
    public bool doIattack = false;//modified externally by combat handler
    Collider player;

    // Update is called once per frame
    void Update()
    {
        //attack or else logic
        if (doIattack == true && timer_time >= (hold_time / 2)) //attack
        {

            player = GetComponentInParent<enemy_combat_handler>().player_col;//get pretty much the players position

            if (Physics.Raycast(this.gameObject.transform.position, Vector3.Normalize(player.transform.position - this.gameObject.transform.position), 1000000, 1 << 9) == true)//is the player blocking? - i put "1 << 9" there and not just "9" because that´s how it works, apparently.
            {
                this.gameObject.transform.localScale = new Vector3(0, 0, 0);//if the player is blocking that means i cant enable the damage collider
            }
            else//i can enable the damage collider
            {
                if (normal_attack == true)
                {
                    GetComponentInChildren<ForestGolemHulkAttack>().Disable(); GetComponentInChildren<ForestGolemNormalAttack>().Enable();// scaling damage collider so the attack works 
                }
                else
                {
                    GetComponentInChildren<ForestGolemNormalAttack>().Disable(); GetComponentInChildren<ForestGolemHulkAttack>().Enable(); // scaling damage collider so the attack works 
                }
                this.gameObject.transform.localScale = new Vector3(1, 1, 1);
            }

        }
        else //be chill
        {
            this.gameObject.transform.localScale = new Vector3(0, 0, 0);//scale the whole group of colliders so they dont affect the player

        }

        //advance timer or else logic
        if (timer_time >= hold_time)
        {
            timer_time = 0;
            ToggleAttackType();//this looks like a good moment to toggle the attack type because it only happens once every timer cycle
        }
        else
        {
            if (doIattack == true) //this "if" prevents the timer from updating when not being used
            {
                timer_time += (Time.deltaTime) / 2;
            }
            else
            {
                timer_time = 0; //resets timer when not in use so it waits for attacking all over again when player reencounters this enemy instance
            }
        }
    }

    void ToggleAttackType()
    {
        if (normal_attack == true)
        {
            normal_attack = false;
        }
        else
        {
            normal_attack = true;
        }
    }
}
