﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turret_shoot : MonoBehaviour
{
    //this script is meant to be in a turret, which has to have:
    //
    //* a child object with a box collider
    //* a projectile asset associated
    //

    public bool get_shootin = false;
    public float shoot_time = 1;
    public float timer = 0;
    public GameObject projectile;
 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (get_shootin==true)
        {
            if (timer >= shoot_time)
            {
                timer = 0;
                Instantiate(projectile , gameObject.GetComponentInChildren<BoxCollider>().transform.position , gameObject.transform.rotation );
            }
            else
            {
                timer += Time.deltaTime * 1;
            }

        }
    }

     public void StartTheShooting(Collider other)
    {
       
            print("shoot");
            get_shootin = true;
        
    }
    public void StopTheShooting(Collider other)
    {
        
            get_shootin = false;
        
    }
}
