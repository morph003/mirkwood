﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class proyectile_homing : MonoBehaviour
{
    public Vector3 homing_vector = new Vector3(0,0,1);
    public float timer = 0;
    public float destroy_timer=2;
    public GameObject turret;
    public float speed = 1;
    // Start is called before the first frame update
    void Start()
    {
      // homing_vector = this.gameObject.transform.position
    }

    // Update is called once per frame
    void Update()
    {
        homing_vector = gameObject.transform.rotation * new Vector3(0, 0, 1);
        homing_vector = Vector3.Normalize(homing_vector);
            // homing_vector = Vector3.Normalize( this.gameObject.transform.position - turret.gameObject.transform.position);
        this.gameObject.transform.position += homing_vector * Time.deltaTime*speed;
        if (timer >= destroy_timer)
        {
            timer = 0;
            Destroy(this.gameObject, 0);
        }
        else
        {
            timer += 1 * Time.deltaTime;
        }
    }
}
