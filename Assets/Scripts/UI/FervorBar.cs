﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FervorBar : MonoBehaviour
{
    public PlayerStats player;
    public Image bar1;
    public Text texto;

    [SerializeField]
    private float lerpSpeed = 2;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {
        
        UpdateFervor();
    }

    void UpdateFervor()
    {
    
            bar1.fillAmount = Mathf.Lerp(bar1.fillAmount, player.stamina /player.maxstamina, Time.deltaTime * lerpSpeed);
            texto.text = player.stamina + "/" + player.maxstamina;
    }
}
