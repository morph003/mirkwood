﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PicoHielo : MonoBehaviour
{
    public bool destroyAll = false;
    private PlayerController playerController;
    private Collider collider;    
    private bool canDamage = true;

    void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();  
        collider = GetComponent<Collider>();
    }

    void Update()
    {
        if (destroyAll) {
            Destroy(this.gameObject);
        }    
    }

    private void OnTriggerStay(Collider other) {
        if (other.gameObject.tag=="Player") {
            if (canDamage) {
                playerController.TakeDamage(10f);
                Debug.Log("picotazo");
                canDamage = false;
            }            
        }
    }

}
