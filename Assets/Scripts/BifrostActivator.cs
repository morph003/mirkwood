﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BifrostActivator : MonoBehaviour
{

    public GameObject player;
    public GameObject reposition;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            CharacterController comp = collision.gameObject.GetComponent<CharacterController>();
            comp.enabled = false;
            player.transform.position = reposition.transform.position;
            comp.enabled = true;

        }
    }
}
