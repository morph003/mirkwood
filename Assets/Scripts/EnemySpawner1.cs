﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner1 : MonoBehaviour
{
    public Vector3[] reposition;
    int[] enemy_quantity = new int[] {1,2,3,3,4,3,4,5,6,4,5};

    public GameObject Enemy1;
    public GameObject Player;
    float dist;
    int i;
    int i_enemies;
    // Start is called before the first frame update
    void Start()
    {
        i = -1;
        i_enemies = 0;
    }

    void SpawnEnemies(int enemy_amount)
    {
        if (enemy_amount == 0)
        {
        }
        else if (enemy_amount == 1)
        {
            Instantiate(Enemy1, transform.position, transform.rotation);
        }
        else if (enemy_amount == 2)
        {
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-2.5f, 2.5f), transform.position.y, transform.position.z + Random.Range(-2.5f, 2.5f));
            Instantiate(Enemy1, transform.position, transform.rotation);
        }
        else if (enemy_amount == 3)
        {
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-2.5f, 2.5f), transform.position.y, transform.position.z + Random.Range(-2.5f, 2.5f));
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-2.5f, 2.5f), transform.position.y, transform.position.z + Random.Range(-2.5f, 2.5f));
            Instantiate(Enemy1, transform.position, transform.rotation);
        }
        else if (enemy_amount == 4)
        {
            Instantiate(Enemy1,transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-2.5f, 2.5f), transform.position.y, transform.position.z + Random.Range(-2.5f, 2.5f));
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-2.5f, 2.5f), transform.position.y, transform.position.z + Random.Range(-2.5f, 2.5f));
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-2.5f, 2.5f), transform.position.y, transform.position.z + Random.Range(-2.5f, 2.5f));
            Instantiate(Enemy1, transform.position, transform.rotation);
            
        }
        else if (enemy_amount == 5)
        {
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-2.5f, 2.5f), transform.position.y, transform.position.z + Random.Range(-2.5f, 2.5f));
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-2.5f, 2.5f), transform.position.y, transform.position.z + Random.Range(-2.5f, 2.5f));
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-2.5f, 2.5f), transform.position.y, transform.position.z + Random.Range(-2.5f, 2.5f));
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-2.5f, 2.5f), transform.position.y, transform.position.z + Random.Range(-2.5f, 2.5f));
            Instantiate(Enemy1, transform.position, transform.rotation);
        }
        else if (enemy_amount == 6)
        {
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-2.5f, 2.5f), transform.position.y, transform.position.z + Random.Range(-2.5f, 2.5f));
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-2.5f, 2.5f), transform.position.y, transform.position.z + Random.Range(-2.5f, 2.5f));
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-2.5f, 2.5f), transform.position.y, transform.position.z + Random.Range(-2.5f, 2.5f));
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-2.5f, 2.5f), transform.position.y, transform.position.z + Random.Range(-2.5f, 2.5f));
            Instantiate(Enemy1, transform.position, transform.rotation);
            transform.position = new Vector3(transform.position.x + Random.Range(-2.5f, 2.5f), transform.position.y, transform.position.z + Random.Range(-2.5f, 2.5f));
            Instantiate(Enemy1, transform.position, transform.rotation);
        }
    }

    // Update is called once per frame
    void Update()
    {
  
        float dist = Vector3.Distance(Player.transform.position, transform.position);
        if (dist<40)
        {
            SpawnEnemies(enemy_quantity[i_enemies]);
            i++;
            transform.position = reposition[i];
            i_enemies++;          
        }
    }
}
