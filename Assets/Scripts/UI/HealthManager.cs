﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour
{
    public PlayerStats player;
    //public PlayerController playerc;
    public Image hb;
    public Text texto;
    [SerializeField]
    private float lerpSpeed=2;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        //playerc = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();


    }

    // Update is called once per frame
    void Update()
    {
        UpdateHeal(player.life);
    }

    void UpdateHeal(float health)
    {
        
            hb.fillAmount = Mathf.Lerp(hb.fillAmount, health / player.maxlife,Time.deltaTime*lerpSpeed);
            texto.text = player.life + "/" + player.maxlife;

    }

}
