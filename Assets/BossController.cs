﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    public GameObject pico;
    public GameObject player;
    private Animator animator;
    private SimpleCinemachineShake cinemachineShake;

    void Start()
    {
        player=GameObject.FindGameObjectWithTag("Player");
        cinemachineShake = GetComponent<SimpleCinemachineShake>();
        animator = gameObject.GetComponent<Animator>();
        StartCoroutine(Idle());        
        cinemachineShake.Shake(3.6f);
    }

    IEnumerator Idle() {
        animator.SetTrigger("Idle");
        yield return new WaitForSeconds(5f);   
        StartCoroutine(Hielo());
    }  

    IEnumerator Hielo() {
        animator.SetTrigger("AtaqueHielo");
        yield return new WaitForSeconds(2f); 
        cinemachineShake.Shake(6.8f);
        Instantiate(pico, player.transform.position, Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0)));
        yield return new WaitForSeconds(0.6f);
        Instantiate(pico, player.transform.position, Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0)));
        yield return new WaitForSeconds(0.6f);
        Instantiate(pico, player.transform.position, Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0)));
        yield return new WaitForSeconds(0.6f);
        Instantiate(pico, player.transform.position, Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0)));
        yield return new WaitForSeconds(0.6f);
        Instantiate(pico, player.transform.position, Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0)));
        yield return new WaitForSeconds(0.6f);
        Instantiate(pico, player.transform.position, Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0)));
        yield return new WaitForSeconds(0.6f);
        animator.SetTrigger("Taunt01");
        Instantiate(pico, player.transform.position, Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0)));
        yield return new WaitForSeconds(0.6f);
        Instantiate(pico, player.transform.position, Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0)));
        yield return new WaitForSeconds(0.6f);
        Instantiate(pico, player.transform.position, Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0)));
        yield return new WaitForSeconds(0.6f);
        Instantiate(pico, player.transform.position, Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0)));

        StartCoroutine(Idle());
    }
}
