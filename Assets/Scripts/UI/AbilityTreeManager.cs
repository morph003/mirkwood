﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityTreeManager : MonoBehaviour
{
    public AbilityButton[] botones;
    public int index = 0;
    
    
    // Start is called before the first frame update
    void Start()
    {
        botones[index].isHighlighted = true;
        
    }

    // Update is called once per frame
    void Update()
    {
        

        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {

            if (index % 2 != 0)
            {
                botones[index].isHighlighted = false;
                index = index -1;
                botones[index].isHighlighted = true;
            }
            else if (index % 2 == 0)
            {
                botones[index].isHighlighted = false;
                index = index + 1;
                botones[index].isHighlighted = true;
            }
            
        }
        if(Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (index % 2 != 0)
            {
                botones[index].isHighlighted = false;
                index = index - 1;
                botones[index].isHighlighted = true;

            }
            else if (index % 2 == 0)
            {
                botones[index].isHighlighted = false;
                index = index + 1;
                botones[index].isHighlighted = true;
            }
        }
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (index % 2 != 0)
            {
                if(index==9)
                {
                    botones[index].isHighlighted = false;
                    index = 1;
                    botones[index].isHighlighted = true;
                }
                else
                {
                    botones[index].isHighlighted = false;
                    index = index + 2;
                    botones[index].isHighlighted = true;
                }

            }else if (index % 2 == 0)
            {
                if (index == 8)
                {
                    botones[index].isHighlighted = false;
                    index = 0;
                    botones[index].isHighlighted = true;
                }
                else
                {
                    botones[index].isHighlighted = false;
                    index = index + 2;
                    botones[index].isHighlighted = true;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (index % 2 != 0)
            {
                if (index == 1)
                {
                    botones[index].isHighlighted = false;
                    index = 9;
                    botones[index].isHighlighted = true;
                }
                else
                {
                    botones[index].isHighlighted = false;
                    index = index - 2;
                    botones[index].isHighlighted = true;
                }

            }
            else if (index % 2 == 0)
            {
                if (index == 0)
                {
                    botones[index].isHighlighted = false;
                    index = 8;
                    botones[index].isHighlighted = true;
                }
                else
                {
                    botones[index].isHighlighted = false;
                    index = index - 2;
                    botones[index].isHighlighted = true;
                }
            }
        }
    }
}
