﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class chase : MonoBehaviour
{
    public Rigidbody baddie_body;
    public NavMeshAgent baddie_soul;
    public bool is_player_near_me=false;
    public GameObject player_ref;
    public Vector3 player_pos = new Vector3 (0,0,0);
   // public float baddie_height=0;//deprecated

    public bool go_back_to_patrol_button = false;

    // Start is called before the first frame update
    void Start()
    {
        player_ref = GameObject.FindGameObjectWithTag("Player");
        baddie_body = gameObject.GetComponent<Rigidbody>();
        baddie_soul = gameObject.GetComponentInChildren<NavMeshAgent>();
       // baddie_height = gameObject.GetComponent<patrol_test>().patrol_target_height;//deprecated
    }

    // Update is called once per frame
    void Update()
    {
        player_pos = player_ref.gameObject.transform.position;

        if (is_player_near_me == true)
        {
            baddie_soul.destination = player_pos;
            gameObject.GetComponent<enemy_move>().activate_chase_mode = true;
           
        }

        if (go_back_to_patrol_button == true) //this button is for testing purposes
        {
            GoBackToPatrol();
            go_back_to_patrol_button = false;
        }
    }

    public void GoBackToPatrol()
    {
        is_player_near_me = false;
        gameObject.GetComponent<enemy_move>().activate_chase_mode = false;
        gameObject.GetComponent<patrol_test>().run_timer = true;
    }
}
