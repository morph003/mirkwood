﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    private Animator anim;
    public float stamina=100;
    public float maxstamina;
    public int essence;
    public bool stance=true;
    public bool selectedbranch;
    public float maxlife=100;
    public float life = 100;
    public float damage = 10;
    public float shieldlife=100;
    public float weaponKnockback;
    public float shieldKnockback;
    public bool stanceUpgrade1 = false;
    public bool stanceUpgrade2 = false;
    public bool stanceUpgrade3 = false;
    public bool stanceUpgrade4 = false;
    public static float altarcount = 0;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        life = maxlife;
        stamina = maxstamina;
    }

    // Update is called once per frame
    void Update()
    {
        if (life < 0)
        {
            life = 0;
        }

        
        if(Input.GetKeyDown(KeyCode.Space)){
            Debug.Log("Stance check");
           if(stance==false){
               Debug.Log("Stance defensa on");
            stance=true;
                anim.SetBool("Stance", true);
            return;
           }
           
            if(stance==true){
                Debug.Log("Stance ataque on");
            stance=false;
            anim.SetBool("Stance", false);
                return;
            }
            
        }
        //StaminaRegen();
    }
    void StaminaRegen()
    {
        stamina += 1f * Time.deltaTime;
        if (stamina >= 100)
            stamina = 100;
    }
    public void Upgrade1()
    {
        if (selectedbranch == false)
        {
            damage = damage * 1.20f;
        }
        if (selectedbranch == true)
        {
            maxlife = life * 1.20f;
        }
    }
    void Upgrade2()
    {
        if (selectedbranch == false)
        {
            damage = damage * 1.20f;
        }
        if (selectedbranch == true)
        {
            maxlife = life * 1.20f;
        }

    }
    void Upgrade3()
    {
        if (selectedbranch == false)
        {
            damage = damage * 1.20f;
        }
        if (selectedbranch == true)
        {
            maxlife = life * 1.20f;
        }

    }
    void Upgrade4()
    {
        if (selectedbranch == false)
        {
            damage = damage * 1.20f;
        }
        if (selectedbranch == true)
        {
            maxlife = life * 1.20f;
        }

    }

}
