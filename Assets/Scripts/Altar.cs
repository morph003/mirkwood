﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Altar : MonoBehaviour
{
    public GameObject text;
    public GameObject abilitytree;
    public bool buttonpress;
    private bool isPlayer;
    // just count first time interact
    public float count;

    public PlayerController player;

    
    // Start is called before the first frame update
    void Start()
    {
        text.SetActive(false);
        abilitytree.SetActive(false);
        buttonpress = false;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

    }

    // Update is called once per frame
    void Update()
    {
        if (isPlayer && Input.GetKeyDown(KeyCode.E))
        {
            if (buttonpress)
            {
                player.onMenu = false;
                buttonpress = false;
                abilitytree.SetActive(false);

                Time.timeScale = 1;
            }
            else if (!buttonpress)
            {

                if (count <= 0)
                {
                    PlayerStats.altarcount++;
                    count++;
                }
                buttonpress = true;
                player.onMenu = true;
                text.SetActive(false);
                abilitytree.SetActive(true);
                Time.timeScale = 0;
            }
        }

        
    }
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "Player")
        {
            text.SetActive(true);
            isPlayer = true;
           
        }
    }
    void OnTriggerStay(Collider other)
    {
       
        
            //other.gameObject.GetComponent<PlayerStats>().Upgrade1();
          
        if (other.tag == "Player" )
        {
            text.SetActive(true);

        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            text.SetActive(false);
            isPlayer = false;
        }
    }
}
