﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarlySpectrWeaponTransformHandler_anim : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AnimAttackEnableStart()
    {
        GetComponentInChildren<EarlySpectrWeaponTransformHandler>().AnimAttackEnableEnd();
    }

    public void AnimAttackDisableStart()
    {
        GetComponentInChildren<EarlySpectrWeaponTransformHandler>().AnimAttackDisableEnd();
    }

}
