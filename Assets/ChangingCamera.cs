﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ChangingCamera : MonoBehaviour
{
    public CinemachineVirtualCamera gameplayCamera;
    public GameObject boss;
    private MovementInput playerMovement;

    private void Start() {
        playerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<MovementInput>();
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Player")) {
            gameplayCamera.m_Priority=1;
            StartCoroutine(DisableMovement());
        }
    }

    private IEnumerator DisableMovement() {
        yield return new WaitForSeconds(0.2f);
        boss.SetActive(true);
    }
}
