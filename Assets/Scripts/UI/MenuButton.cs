﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuButton : MonoBehaviour
{
    public string scene;
        
    public bool isHighlighted = false;

    private bool isPress = false;
    private float delay = 0.5f;
    private float Tiempo;

    Image buttonref;

    public GameObject menupausa;

    // Start is called before the first frame update
    void Start()
    {

        Tiempo = delay;
        buttonref = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isHighlighted)
        {

            buttonref.color = Color.yellow;

        }
        else
        {
            buttonref.color = Color.grey;
        }

        if (Input.GetKeyDown(KeyCode.Return) && isHighlighted)
        {

            isPress = true;


            if (Tiempo <= 0) { SceneManager.LoadScene(scene, LoadSceneMode.Single); }

        }
        if (isPress)
        {
            Tiempo -= Time.unscaledDeltaTime; //Se usa el unscaleDeltaTime para que Time.Scale no afecte el proceso
            if (Tiempo <= 0)
            {
                Tiempo = delay;
                isPress = false;

                if (scene == "Exit")
                {
                    Application.Quit();
                    return;
                }
              

                SceneManager.LoadScene(scene, LoadSceneMode.Single);
            }

        }
    }
}
