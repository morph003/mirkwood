﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonEvents : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void InicialButton()
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }

    public void Jugar()
    {
        SceneManager.LoadScene("WhiteblockingGeneral", LoadSceneMode.Single);
    }

    public void Boss()
    {
        SceneManager.LoadScene("escenaBoss", LoadSceneMode.Single);
    }

    public void Salir()
    {
        Application.Quit();
    }

}
