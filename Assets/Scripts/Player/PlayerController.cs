﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public bool isDead;
    private Rigidbody rb;
    private PlayerAttack pa;

    private PlayerStats ps;
    private MovementInput mi;
    public bool onMenu;
    // Start is called before the first frame update
    void Start()
    {
        rb=GetComponent<Rigidbody>();
        ps = GetComponent<PlayerStats>();
        pa = GetComponent<PlayerAttack>();
        mi = GetComponent<MovementInput>();
        isDead = false;






    }

    // Update is called once per frame
       void Update() {
        if (onMenu)
        {
          
            ps.enabled = false;
            pa.enabled = false;
            mi.enabled = false;
        }
        if (!onMenu)
        {
            ps.enabled = true;
            pa.enabled = true;
            mi.enabled = true;
        }
        if (isDead)
        {
            ps.enabled = false;
            pa.enabled = false;
            mi.enabled = false;
        }

        }

 
      
public void TakeDamage(float dam)
{
    ps.life -= dam;
    Debug.Log("Recibido " + dam + " daño");
  
    if (ps.life <= 0)
    {
            isDead = true;
        Debug.Log("Die");
    }
}
    public void TakeShieldDamage(float dam)
    {
        float damageLeft = dam;
        if (ps.shieldlife > 0)
        { 
            
            float shieldDamage = Mathf.Min(damageLeft, ps.shieldlife);
            Debug.Log(shieldDamage);
            ps.shieldlife -= shieldDamage;
            damageLeft -= shieldDamage;
            Debug.Log(damageLeft);
        }

        if (damageLeft >= 0)
        {
            TakeDamage(damageLeft);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Esencia")
        {
            ps.essence++;

            Destroy(other.gameObject);
        }
    }

}

     


    

